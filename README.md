# KGÜ 5: Assembler

### Inhalte

Assembler, Aufbau des Objektformates

### Lernziele

Assemblieren eines gegebenen Programms

## Aufgabe 1: Die Erzeugung des Objektcodes

In dieser Aufgabe sollen Sie die Arbeit eines Assemblers anhand des folgenden Bei-
spielprogramms für einen vereinfachten Rechner nachvollziehen, der dem x86 ähnlich
ist. Das Befehlsformat ist gegenüber dem x86 wie folgt vereinfacht und besteht aus
folgenden Elementen:

1. OpCode (1 Byte)
2. Register- und Adressmodusbezeichner des 1. Operanden (1 Byte, optional)
3. Register- und Adressmodusbezeichner des 2. Operanden (1 Byte, optional)
4. SI-Byte (Abkürzung steht für Scale und Index, 1 Byte, optional)
5. Displacement (4 Bytes, optional)
6. Immediate (4 Bytes, optional)

Anhand der Anweisung `mov edx,[ebx+8*ecx]` wird die Bildung des Maschinencodes
erläutert. Der Opcode ist mit Hilfe der [Opcodes Tabelle](#opcodes) zu
bestimmen und ist in diesem Fall `0c`. Für die beiden Operanden muss
anschließend jeweils ein Register- und Adressmodusbezeichner bestimmt werden,
die jeweils in einem Byte codiert werden. Die Bits 0-3 des Bezeichners
spezifizieren den Adressmodus und sind der
[Adressmodus Tabelle](#adressmodus-untere-4-bits) zu entnehmen. Der erste
Operand stellt ein Register dar, so dass die Bits 0-3 den Wert 0 annehmen. Die
Bits 4-7 beschreiben, welches Register als Basis für diese Operation verwendet
wird und nimmt in diesem Fall den Wert 3 an, der für das Register edx steht.
Somit besitzt der Register- und Adressmodusbezeichner des ersten Operanden den
Wert 30. Der zweite Operand verwendet als Adressierungsmodus „indirekt
indiziert“, so dass die Bits 0-3 des Register- und Adressmodusbezeichners den
Wert 4 annehmen. Die Bits 4 bis 7 nehmen den Wert 1 an, der für das Register
`ebx` steht. Somit besitzt der Register- und Adressmodusbezeichner des zweiten
Operanden den Wert 14. Anschließend müssen im SI-Byte sowohl das Index-Register
als auch der Skalierungsfaktor des zweiten Operanden codiert werden. Bits 0-3
stehen hierbei für das Index-Register und nehmen somit den Wert 2 an. Die Bits
4-7 beschreiben den Skalierungsfaktor und nehmen hier den Wert 8 an. Somit
ergibt sich für die Anweisung `mov edx,[ebx+8*ecx]` folgender Maschinencode:
`0c301482`.

```nasm
extern printf
extern init_pic

global invert
global main

SECTION .text
invert:
        push    ebp
        mov     ebp, esp
        push    ebx

        push    message
        call    printf
        add     esp,4

        mov     eax,[ebp+16]
        imul    eax,[ebp+12]
        imul    eax,3
        mov     ecx,0
        mov     ebx,[ebp+8]

.start:
        cmp     ecx,eax
        jge     .end
        not     byte [ebx+ecx]
        inc     ecx
        jmp     .start

.end:
        pop     ebx
        pop     ebp
        ret

main:
        push    ebp
        mov     ebp,esp

        mov     eax,height
        imul    eax,width
        imul    eax,3

        push    eax
        push    pic
        call    init_pic
        add     esp,8

        push    width
        push    height
        push    pic
        call    invert
        add     esp,12

        pop     ebp
        mov     eax,1
        mov     ebx,0
        int     0x80

SECTION .bss
height  equ     4
width   equ     4
pic     resb    heigh * width * 3

SECTION .data
message db      `Inverting picture.\n`,0
```

### a)

Erläutern Sie _kurz_ die Funktionsweise des Beispielprogrammes.

### b)

Vollziehen Sie die Phase 1 eines Assemblers wie in der Vorlesung nach. Geben
Sie die Pseudooperationen und Maschinenoperationen an. Errechnen Sie die Länge
der einzelnen Befehle in Maschinencode und geben Sie diese an. Schreiben Sie zu
jeder Zeile den dazugehörigen Locationcounter. Generieren Sie die Symboltabelle
des Assemblers, und geben Sie die Länge der Code- und Datensektion an. Die
Datensektion kann an einer beliebigen Adresse beginnen.

#### Symboltabelle

| Symbol       | Typ  | Wert   | Global sichtbar ? (Ja/Nein) | Weitere Attribute |
| ------------ | ---- | ------ | --------------------------- | ----------------- |
|  |  |  |  |  |
|  |  |  |  |  |
|  |  |  |  |  |
|  |  |  |  |  |
|  |  |  |  |  |
|  |  |  |  |  |
|  |  |  |  |  |
|  |  |  |  |  |
|  |  |  |  |  |
|  |  |  |  |  |

### c)

Vollziehen Sie nun die Phase 2 des Assemblers nach, indem Sie ein Objekt im
vorgestellten Objektformat erzeugen. Zur Vereinfachung können alle Zahlen im
„big-endian Format“ dargestellt werden. Außerdem seien alle Sprünge (jumps)
relativ und alle Funktionsaufrufe (calls) absolut! Kodieren Sie genau drei
Befehle in einem T-Datensatz (soweit möglich)! Nutzen Sie den S-Datensatz!
Gehen Sie davon aus, dass der Dateiname und damit auch der Modulname
`test_invert` lautet!

#### Objektcode

```
```

### Tabellen

#### Opcodes

| Mnemonic | OpCode |
| -------- | ------ |
| add      | 0x01   |
| sub      | 0x02   |
| inc      | 0x03   |
| dec      | 0x04   |
| push     | 0x05   |
| pop      | 0x06   |
| jmp      | 0x07   |
| call     | 0x08   |
| ret      | 0x09   |
| cmp      | 0x0A   |
| je       | 0x0B   |
| mov      | 0x0C   |
| int      | 0x0D   |

#### Adressmodus (untere 4 Bits)

| Adressierungsart     | Adressmodus | Beispiele                |
| -------------------- | ----------- | ------------------------ |
| register             | 0x0         | `mov eax, ...`           |
| immediate            | 0x1         | `push 2000`              |
| direkt               | 0x2         | `mov [2000], ...`        |
| indirekt             | 0x3         | `mov [ebx], ...`         |
| indirekt indiziert   | 0x4         | `mov [ebx+8*eax], ...`   |
| indir. mit disp.     | 0x5         | `mov [eax-4], ...`       |
| indir. ind. m. disp. | 0x6         | `mov [eax+8*ebx-4], ...` |

#### Register (obere 4 Bits)

| Register | Bezeichner |
| --------:| ---------- |
|    `eax` | 0x0        |
|    `ebx` | 0x1        |
|    `ecx` | 0x2        |
|    `edx` | 0x3        |
|    `ebp` | 0x4        |
|    `esp` | 0x5        |
|    `edi` | 0x6        |
|    `esi` | 0x7        |
|     `ax` | 0x8        |
|     `al` | 0x9        |
|     `ah` | 0xA        |
|     `bx` | 0xB        |
|     `bl` | 0xC        |
|     `bh` | 0xD        |
